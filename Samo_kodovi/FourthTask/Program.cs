﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FourthTask
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager myManager = new NotificationManager();
            ConsoleColor currentForeground = Console.ForegroundColor;
            DateTime time = new DateTime();
            time = DateTime.Now;
            
            ConsoleNotification firstConsoleNotification = new ConsoleNotification("Iva", "My first title", "My first text.", time, Category.ERROR, currentForeground);
            myManager.Display(firstConsoleNotification);
            
            ConsoleNotification secondConsoleNotification = new ConsoleNotification("Iva", "My second title", "My second text.", time, Category.ALERT, currentForeground);
            myManager.Display(secondConsoleNotification);
            
            ConsoleNotification thirdConsoleNotification = new ConsoleNotification("Iva", "My third title", "My third text.", time, Category.INFO, currentForeground);
            myManager.Display(thirdConsoleNotification);
        }
    }
}
