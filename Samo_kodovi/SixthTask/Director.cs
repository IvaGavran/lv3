﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SixthTask
{
    class Director
    {
        IBuilder builder;
        public Director(IBuilder builder) { this.builder = builder; }
        public void SetBuilder(IBuilder builder)
        {
            this.builder = builder;
        }
        public ConsoleNotification CategoryALLERT(String author)
        {
            DateTime time = new DateTime();
            time = DateTime.Now;
            return builder.SetAuthor(author).SetTitle("My allert title").SetTime(time)
            .SetLevel(Category.ALERT).SetColor(Console.ForegroundColor)
            .SetText("My allert text.").Build();
        }
        public ConsoleNotification CategoryERROR(String author)
        {
            DateTime time = new DateTime();
            time = DateTime.Now;
            return builder.SetAuthor(author).SetTitle("My error title").SetTime(time)
            .SetLevel(Category.ERROR).SetColor(Console.ForegroundColor)
            .SetText("My error text.").Build();
        }
        public ConsoleNotification CategoryINFO(String author)
        {
            DateTime time = new DateTime();
            time = DateTime.Now;
            return builder.SetAuthor(author).SetTitle("My info title").SetTime(time)
            .SetLevel(Category.INFO).SetColor(Console.ForegroundColor)
            .SetText("My info text.").Build();
        }
    }
}

