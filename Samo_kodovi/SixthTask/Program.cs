﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SixthTask
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager myManager = new NotificationManager();
            IBuilder builder = new NotificationBuilder();
            Director director = new Director(builder);
            ConsoleNotification consoleNotification = director.CategoryALLERT("Iva");
            myManager.Display(consoleNotification);

            consoleNotification = director.CategoryERROR("Iva");
            myManager.Display(consoleNotification);

            consoleNotification = director.CategoryINFO("Iva");
            myManager.Display(consoleNotification);
        }
    }
}
