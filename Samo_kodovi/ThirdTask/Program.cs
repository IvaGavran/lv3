﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdTask
{
    //Pitanje: Ako je datoteka postavljena na jednom mjestu u tekstu programa, hoće li uporaba loggera na
    //drugim mjesta u testu programa pisati u istu datoteku(pretpostavka je kako nisu ponovo postavljene)?
    //Odgovor: Uporaba loggera na drugim mjestima, pisat će u istu datoteku jer se radi o istoj putanji.
    class Program
    {
        static void Main(string[] args)
        {
            Logger myLogger = Logger.GetInstance();
            myLogger.Log("Iva");
            myLogger.Log("Gavran");

            myLogger.Set("C:\\Users\\Kivi\\source\\repos\\RPPOON_LV3\\ThirdTask\\FileIvaWithNewPath.csv");
            myLogger.Log("New path is set.");
        }
    }
}
