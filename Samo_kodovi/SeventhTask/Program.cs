﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeventhTask
{
    //Pitanje: Ima li u konkretnom slučaju razlike između plitkog i dubokog kopiranja?
    //Odgovor: Ima razlike zbog stringova. String u plitkom kopira samo pokazivač na isti string, zbog toga je potrebno duboko kopiranje.
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager myManager = new NotificationManager();
            ConsoleColor currentForeground = Console.ForegroundColor;
            DateTime time = new DateTime();
            time = DateTime.Now;

            ConsoleNotification myConsoleNotification = new ConsoleNotification("Iva", "My title", "My text.", time, Category.INFO, currentForeground);
            myManager.Display(myConsoleNotification);
            Console.WriteLine();

            Console.WriteLine("Deep copy:");
            ConsoleNotification clonedNotification = (ConsoleNotification)myConsoleNotification.Clone();
            myManager.Display(clonedNotification);
            Console.WriteLine();

            Console.WriteLine("Shallow copy:");
            clonedNotification = (ConsoleNotification)myConsoleNotification.CloneShallow();
            myManager.Display(clonedNotification);
            Console.WriteLine();
        }
    }
}
