﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondTask
{
    //Pitanje: Koliko odgovornosti ima navedena metoda? (CreateRandomMatrix)
    //Odgovor: Ima dvije odgovornosti (kreira matricu te ju puni realnim pseudoslučajnim brojevima), dok klasa ima također više odgovornosti (instanciranje te funkcionalnost).

    class Program
    {
        static void Main(string[] args)
        {       
            int rows = 4;
            int columns = 4;

            MatrixGenerator myMatrixGenerator = MatrixGenerator.GetInstance();
            double[][] myMatrix= myMatrixGenerator.CreateRandomMatrix(rows, columns);

            for (int i = 0; i < myMatrix.Length; i++)
            {
                Console.Write("Element({0}): ", i);
                for (int j = 0; j < myMatrix[i].Length; j++)
                {
                    Console.Write("{0} ", myMatrix[i][j]);
                }
                Console.WriteLine();
            }
        }
    }
}
