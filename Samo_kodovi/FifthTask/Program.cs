﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FifthTask
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime time = new DateTime();
            time = DateTime.Now;
            Category level = Category.ALERT;
            ConsoleColor color = Console.ForegroundColor;
      
            NotificationBuilder builder = new NotificationBuilder();

            builder.SetAuthor("Iva");
            builder.SetTitle("My title");
            builder.SetTime(time);
            builder.SetLevel(level);
            builder.SetColor(color);
            builder.SetText("My text.");

            ConsoleNotification myConsoleNotification = builder.Build();
            Console.WriteLine(myConsoleNotification);
        }
    }
}
